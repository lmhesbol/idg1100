#!/usr/bin/bash

date=$(date +"%Y-%m-%d-%H-%M")

mkdir scraped
cd scraped

echo $date > date

mkdir news-$date
cd news-$date

curl -s  https://www.tv2.no/nyheter > fil.txt

link1=$(cat fil.txt | grep -i '<a class="article__link" ' | head -n 3 | sed -E 's|<a class="article__link" href="|https://www.tv2.no|' | sed -E 's/">//' | head -n 1)
link2=$(cat fil.txt | grep -i '<a class="article__link" ' | head -n 3 | sed -E 's|<a class="article__link" href="|https://www.tv2.no|' | sed -E 's/">//' | head -n 2 | tail -n 1)
link3=$(cat fil.txt | grep -i '<a class="article__link" ' | head -n 3 | sed -E 's|<a class="article__link" href="|https://www.tv2.no|' | sed -E 's/">//' | tail -n 1)

curl -s $link1 > page1
curl -s $link2 > page2
curl -s $link3 > page3

title1=$(cat page1 | grep -i '<h1 itemprop="headline" class="articleheader__title">' | sed -E 's/<h1 itemprop="headline" class="articleheader__title">//' | sed -E 's|</h1>||')
image1=$(cat page1 | grep -i 'https://www.cdn.tv2.no/images' | head -n 2 | tail -n 1 | sed -E 's/<meta property="rnews:thumbnailUrl" content="//' | sed -E 's|[?]*">||')

echo $link1 > scrape1
echo $title1 >> scrape1
echo $image1 >> scrape1
echo $date >> scrape1

title2=$(cat page2 | grep -i '<h1 itemprop="headline" class="articleheader__title">' | sed -E 's/<h1 itemprop="headline" class="articleheader__title">//' | sed -E 's|</h1>||')
image2=$(cat page2 | grep -i 'https://www.cdn.tv2.no/images' | head -n 2 | tail -n 1 | sed -E 's/<meta property="rnews:thumbnailUrl" content="//' | sed -E 's|[?]*">||')

echo $link2 > scrape2
echo $title2 >> scrape2
echo $image2 >> scrape2
echo $date >> scrape2

title3=$(cat page3 | grep -i '<h1 itemprop="headline" class="articleheader__title">' | sed -E 's/<h1 itemprop="headline" class="articleheader__title">//' | sed -E 's|</h1>||')
image3=$(cat page3 | grep -i 'https://www.cdn.tv2.no/images' | head -n 2 | tail -n 1 | sed -E 's/<meta property="rnews:thumbnailUrl" content="//' | sed -E 's|[?]*">||')

echo $link3 > scrape3
echo $title3 >> scrape3
echo $image3 >> scrape3
echo $date >> scrape3

rm page1
rm page2
rm page3 


