#!/bin/bash

currentdir=$(pwd)

cd scraped
temp=$(cat date)
temp1="news-$temp"
cd ..

cd /var/www/html/

mkdir page
cd page

url=$(cat $currentdir/scraped/$temp1/scrape1 | head -n 1)
title=$(cat $currentdir/scraped/$temp1/scrape1 | head -n 2 | tail -n 1)
img=$(cat $currentdir/scraped/$temp1/scrape1 |head -n 3 | tail -n 1)
date=$(cat $currentdir/scraped/$temp1/scrape1 | head -n 4 | tail -n 1)

echo $url > page1.html
echo $title >> page1.html
echo $img >> page1.html
echo $date >> page1.html

echo "<!DOCTYPE html>" > page1.html
echo "<html>" >> page1.html
echo "<head>" >> page1.html
echo "<meta charset=\"UTF-8\">" >> page1.html
echo "<title>$title</title>" >> page1.html
echo "</head>" >> page1.html
echo "<body>" >> page1.html
echo "<h1>$title</h1>" >> page1.html
echo "<div><img src=\"$img\"/></div>" >> page1.html
echo "<p>Fetched on $date <a href=\"$url\">Orginal article</a></p>" >> page1.html
echo "</body>" >> page1.html
echo "</html>" >> page1.html

url=$(cat $currentdir/scraped/$temp1/scrape2 | head -n 1)
title=$(cat $currentdir/scraped/$temp1/scrape2 | head -n 2 | tail -n 1)
img=$(cat $currentdir/scraped/$temp1/scrape2 |head -n 3 | tail -n 1)
date=$(cat $currentdir/scraped/$temp1/scrape2 | head -n 4 | tail -n 1)

echo $url > page2.html
echo $title >> page2.html
echo $img >> page2.html
echo $date >> page2.html

echo "<!DOCTYPE html>" > page2.html
echo "<html>" >> page2.html
echo "<head>" >> page2.html
echo "<meta charset=\"UTF-8\">" >> page2.html
echo "<title>$title</title>" >> page2.html
echo "</head>" >> page2.html
echo "<body>" >> page2.html
echo "<h1>$title</h1>" >> page2.html
echo "<div><img src=\"$img\"/></div>" >> page2.html
echo "<p>Fetched on $date <a href=\"$url\">Orginal article</a></p>" >> page2.html
echo "</body>" >> page2.html
echo "</html>" >> page2.html

url=$(cat $currentdir/scraped/$temp1/scrape3 | head -n 1)
title=$(cat $currentdir/scraped/$temp1/scrape3 | head -n 2 | tail -n 1)
img=$(cat $currentdir/scraped/$temp1/scrape3 |head -n 3 | tail -n 1)
date=$(cat $currentdir/scraped/$temp1/scrape3 | head -n 4 | tail -n 1)

echo $url > page3.html
echo $title >> page3.html
echo $img >> page3.html
echo $date >> page3.html

echo "<!DOCTYPE html>" > page3.html
echo "<html>" >> page3.html
echo "<head>" >> page3.html
echo "<meta charset=\"UTF-8\">" >> page3.html
echo "<title>$title</title>" >> page3.html
echo "</head>" >> page3.html
echo "<body>" >> page3.html
echo "<h1>$title</h1>" >> page3.html
echo "<div><img src=\"$img\"/></div>" >> page3.html
echo "<p>Fetched on $date <a href=\"$url\">Orginal article</a></p>" >> page3.html
echo "</body>" >> page3.html
echo "</html>" >> page3.html
